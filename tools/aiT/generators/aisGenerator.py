# -*- coding: utf-8 -*-

from analysis.program import *
from analysis.PDST import *
from analysis.CFG import *
from aiT.aiTAnalyses import aiTCfgAnalysis

from aiT.reader import AiTXmlReader

""" TODO : change the place of these 2 functions """
def performAiTCfgAnalysis(project, P, analysisName):
	cfgAitProject = project.genNewAiTProject(analysisName)

	cfgAitProject.apx.addExecutable(P.binaryPath)

	cfgAnalysis = aiTCfgAnalysis(analysisName)
	cfgAnalysis.start = "main"
	cfgAnalysis.xmlReport = "%s.xml" % analysisName

	cfgAitProject.addAnalysis(cfgAnalysis)

	cfgAitProject.run()

	xmlResult = AiTXmlReader.readXml(cfgAnalysis.getReportPath('xml'))
	cfgAitProject.results[analysisName]['xml'] = xmlResult

	xmlResult.loopExtraction()
